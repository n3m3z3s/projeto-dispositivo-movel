import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Usuarios } from 'src/Models/Usuarios';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario:Usuarios
  nomeuser:string
  constructor(public fbauth:AngularFireAuth,
    public alertController: AlertController,
    public route:Router) {
      this.usuario=new Usuarios()
     }
 
  ngOnInit() {
  }
  validateInputs() {
    console.log(this.fbauth);
    let uemail = this.usuario.email.trim();
    let usenha = this.usuario.senha.trim();
    return (
      this.usuario.nome &&
      this.usuario.senha &&
      this.usuario.nome.length > 0 &&
      this.usuario.senha.length > 0
    );
  } 

  LoginUsuario()
  {
    this.fbauth.auth.signInWithEmailAndPassword(this.usuario.email,this.usuario.senha).then((res)=>
    {
       this.route.navigate(['/home'])
    })
    .catch(async ()=>
    {
      const alert = await this.alertController.create({
        header: 'Erro',
        subHeader: '',
        message: 'Erro no Login.',
        buttons: ['OK']
      });
  
      await alert.present();

    })
  }

}
